﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour{

    public float speed;
    public Text countText;
    public Text WinText;
    public int jumpCount;
    private Rigidbody rb;
    private int count;
    
    private void Start()
        
    {
        
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        WinText.text = "";
        jumpCount = 0;
        
    }

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce (movement * speed);

        jump();
        onGround();
    }

    void OnTriggerEnter(Collider other)
    {
       if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if ( count >= 16)
        {
            WinText.text = " You win! ";
        }

       
    }

    void jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && jumpCount < 2)
        {
            float jumpDistance = 1.0f;
            transform.position += Vector3.up * jumpDistance;
            jumpCount = jumpCount + 1;
        }
    }

    void onGround()
    {
        if (transform.position.y == 0.5)
        {
            jumpCount = 0;
        }
    }
}
